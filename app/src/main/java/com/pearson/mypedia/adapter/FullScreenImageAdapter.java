package com.pearson.mypedia.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.pearson.mypedia.R;
import com.pearson.mypedia.activity.FullImageViewActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Kaushal on 9/15/2016.
 */
public class FullScreenImageAdapter extends PagerAdapter {
    private static final int MEDIA_TYPE_IMAGE = 1;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Integer> imgPathList;
    private int height, width;

    public FullScreenImageAdapter(FullImageViewActivity fullScreenActivity, ArrayList<Integer> imgPathList) {
        this.context = fullScreenActivity;
        this.imgPathList = imgPathList;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        fullScreenActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
    }

    @Override
    public int getCount() {
        return imgPathList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);
        final ImageView imgDisplay;
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        final LinearLayout linearLayout;
        linearLayout = (LinearLayout) viewLayout.findViewById(R.id.bottom_detail_ll);


        int imgId = imgPathList.get(position);

            Picasso.get().load(imgId).into(imgDisplay);

        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}
