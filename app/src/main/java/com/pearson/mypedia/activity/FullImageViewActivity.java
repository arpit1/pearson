package com.pearson.mypedia.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.pearson.mypedia.BaseActivity;
import com.pearson.mypedia.R;
import com.pearson.mypedia.adapter.FullScreenImageAdapter;

import java.util.ArrayList;

public class FullImageViewActivity extends BaseActivity {
    public static String PARAM_IMAGES = "param images";
    private ViewPager viewPager;
    private FullScreenImageAdapter fullScreenImageAdapter;
    private ArrayList<Integer> imgPathList;
    private ImageView mBackwardIV, mForwardIV, mCancelIV;
    private int mCurrentPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mBackwardIV = (ImageView) findViewById(R.id.iv_backward);
        mForwardIV = (ImageView) findViewById(R.id.iv_forward);
        mCancelIV = (ImageView) findViewById(R.id.iv_cancel);
        imgPathList = (ArrayList<Integer>) getIntent().getSerializableExtra(PARAM_IMAGES);
        clickListners();

        fullScreenImageAdapter = new FullScreenImageAdapter(this, imgPathList);
        viewPager.setAdapter(fullScreenImageAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mCurrentPosition = position;
                if (position == imgPathList.size() - 1) {
                    mForwardIV.setVisibility(View.GONE);
                    mBackwardIV.setVisibility(View.GONE);
                    if (imgPathList.size() > 1)
                        mBackwardIV.setVisibility(View.VISIBLE);
                } else if (position == 0) {
                    mBackwardIV.setVisibility(View.GONE);
                    if (imgPathList.size() > 1)
                        mForwardIV.setVisibility(View.VISIBLE);
                } else {
                    mBackwardIV.setVisibility(View.VISIBLE);
                    mForwardIV.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void clickListners() {
        mBackwardIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(mCurrentPosition - 1);
            }
        });

        mForwardIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(mCurrentPosition + 1);
            }
        });

        mCancelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
