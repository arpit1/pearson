package com.pearson.mypedia.activity

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.pearson.mypedia.BaseActivity
import com.pearson.mypedia.R
import com.pearson.mypedia.database.CSVWriter
import com.pearson.mypedia.database.DBHalper
import com.pearson.mypedia.fragment.*
import com.pearson.mypedia.fragment.cbse.*
import com.pearson.mypedia.interfaces.OnFragmentInteractionListener
import com.pearson.mypedia.models.RequestDetails
import com.pearson.mypedia.utility.Constants
import com.pearson.mypedia.utility.PermissionHelper
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import java.io.File
import java.io.FileWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener, Constants, View.OnClickListener {

    internal var args: Bundle? = null
    //    var btnSubmit: Button? = null
    var etFName: EditText? = null
    var etinstituteName: EditText? = null
    var etPhone: EditText? = null
    var etEmail: EditText? = null
    var etCityName: EditText? = null
    var dbHalper: DBHalper? = null
    internal var requestDetailsList: List<RequestDetails> = ArrayList()
    var isAnimation = true
    var isTab: Boolean = true
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setSupportActionBar(toolbar)
        dbHalper = DBHalper(this)

        setUpToolbar("", false)

        etFName = findViewById(R.id.etFName)
        etinstituteName = findViewById(R.id.etinstituteName)
        etPhone = findViewById(R.id.etPhone)
        etEmail = findViewById(R.id.etEmail)
        etCityName = findViewById(R.id.etCityName)

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        val densityDpi = (displayMetrics.density * 160f)

        Log.e("Screenwidth", "" + width)
        Log.e("Screenheight", "" + height)
        Log.e("ScreenDPI", "" + densityDpi)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.isDrawerIndicatorEnabled = false
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        ivRequestDemo.setOnClickListener(this)
        ivClose.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)

        changeFragment(HOME_FRAGMENT)

        Handler().postDelayed({
            /* Create an Intent that will start the Menu-Activity. */
            topBottomLayoutAnimation(ll_main, ll_main.width - ivRequestDemo!!.width)
//            ll_main.x = (ll_main.width - ivRequestDemo!!.width).toFloat()
            isAnimation = false
        }, 1000)

    }

    fun inIt() {
        if (isTab) {
            Handler().postDelayed({
                /* Create an Intent that will start the Menu-Activity. */
                topBottomLayoutAnimation(ll_main, ll_main.width - ivRequestDemo!!.width)
//            ll_main.x = (ll_main.width - ivRequestDemo!!.width).toFloat()
                isAnimation = false
            }, 1000)
        }
    }

    override fun onResume() {
        super.onResume()
//        val sdf = SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)
//        var d = Date()
//        try {
//            d = sdf.parse("20190421")
//        } catch (ex: ParseException) {
//            Log.v("Exception", ex.localizedMessage)
//        }
//        sdf.applyPattern("dd MMM yyyy")
//        if (Date().time < d.time) {
            if (isTablet(this)) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
                isTab = true
            } else {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                isTab = false
            }
//        } else {
//            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
//        }

        inIt()
    }

    private fun isTablet(context: Context): Boolean {
        return ((context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        } else {
            val count = supportFragmentManager.backStackEntryCount
            if (count == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    return
                }

                this.doubleBackToExitPressedOnce = true

                showSnackBar(drawer_layout, getString(R.string.please_click_back_again))
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
            } else {
                //                getFragmentManager().popBackStackImmediate()
                super.onBackPressed()
//                return
            }
        }

        if (supportFragmentManager.findFragmentByTag("Home Fragment") != null && supportFragmentManager.findFragmentByTag("Home Fragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.color.home_background_color)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("AcademicPlanFragment") != null && supportFragmentManager.findFragmentByTag("AcademicPlanFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.academic_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("AssessMentFragment") != null && supportFragmentManager.findFragmentByTag("AssessMentFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.accessmp)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("StudentFragment") != null && supportFragmentManager.findFragmentByTag("StudentFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.student_app)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("IcseCategoryFragment") != null && supportFragmentManager.findFragmentByTag("IcseCategoryFragment").isVisible) {
            coordinatorLayout.setBackgroundColor(resources.getColor(R.color.home_background_color))
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("EcardFragment") != null && supportFragmentManager.findFragmentByTag("EcardFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.ecard_back)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("StudentCourseFragment") != null && supportFragmentManager.findFragmentByTag("StudentCourseFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.studentcourse)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("TopicWiseFragment") != null && supportFragmentManager.findFragmentByTag("TopicWiseFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.topic_wise)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("TeacherPortal") != null && supportFragmentManager.findFragmentByTag("TeacherPortal").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.teacher_portal)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("HomeAppFragment") != null && supportFragmentManager.findFragmentByTag("HomeAppFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.home_app)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("OnlineProfessionalDevelopment") != null && supportFragmentManager.findFragmentByTag("OnlineProfessionalDevelopment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.onlne_pro)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("DigitalResourcesFragment") != null && supportFragmentManager.findFragmentByTag("DigitalResourcesFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.inclass)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("Contactusfragment") != null && supportFragmentManager.findFragmentByTag("Contactusfragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.contactus_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("BookSampleFragment") != null && supportFragmentManager.findFragmentByTag("BookSampleFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.color.home_background_color)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("RequestADemoFragment") != null && supportFragmentManager.findFragmentByTag("RequestADemoFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.color.home_background_color)
//            super.onBackPressed()
        }

        /*CBSE BACK MANAGE*/
        else if (supportFragmentManager.findFragmentByTag("CbseCategoryFragment") != null && supportFragmentManager.findFragmentByTag("CbseCategoryFragment").isVisible) {
            coordinatorLayout.setBackgroundColor(resources.getColor(R.color.home_background_color))
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CbscAcademicFragment") != null && supportFragmentManager.findFragmentByTag("CbscAcademicFragment").isVisible) {
            // coordinatorLayout.setBackgroundResource(R.drawable.cbse_acedmic)
            coordinatorLayout.setBackgroundResource(R.drawable.chapter)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSETeacherPortal") != null && supportFragmentManager.findFragmentByTag("CBSETeacherPortal").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_teacher_portal)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEOnlineProfessionalDevelopment") != null && supportFragmentManager.findFragmentByTag("CBSEOnlineProfessionalDevelopment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_online)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEHomeAppFragment") != null && supportFragmentManager.findFragmentByTag("CBSEHomeAppFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_home_app_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEDigitalResourcesFragment") != null && supportFragmentManager.findFragmentByTag("CBSEDigitalResourcesFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_inclass)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEStudentFragment") != null && supportFragmentManager.findFragmentByTag("CBSEStudentFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_student_new)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEStudentCourseFragment") != null && supportFragmentManager.findFragmentByTag("CBSEStudentCourseFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_student_course_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEAssessMentFragment") != null && supportFragmentManager.findFragmentByTag("CBSEAssessMentFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_assesment_mapped_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSE_DYNAMIC_LESSON_FRAGMENT") != null && supportFragmentManager.findFragmentByTag("CBSE_DYNAMIC_LESSON_FRAGMENT").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_dynamic_lesson_planning_tool)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSE_INSIGHT_IN_CLASS_FRAGMENT") != null && supportFragmentManager.findFragmentByTag("CBSE_INSIGHT_IN_CLASS_FRAGMENT").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_inside)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSEEcardFragment") != null && supportFragmentManager.findFragmentByTag("CBSEEcardFragment").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.cbse_ereport_card_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSETopicWiseFragment") != null && supportFragmentManager.findFragmentByTag("CBSETopicWiseFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_topic_wise_sla_bg)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSE_AR_VR_FRAGMENT") != null && supportFragmentManager.findFragmentByTag("CBSETopicWiseFragment").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_arvr)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSE_STEM_ENGLISH_FRAGMENT") != null && supportFragmentManager.findFragmentByTag("CBSE_STEM_ENGLISH_FRAGMENT").isVisible) {
            if (isTab)
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_stem)
//            super.onBackPressed()
        } else if (supportFragmentManager.findFragmentByTag("CBSE_CHAPTER_PLAN") != null && supportFragmentManager.findFragmentByTag("CBSE_CHAPTER_PLAN").isVisible) {
            coordinatorLayout.setBackgroundResource(R.drawable.chapter)
//            super.onBackPressed()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivRequestDemo -> {
//                showPop()
                if (isAnimation) {
                    topBottomLayoutAnimation(ll_main, ll_main.width - ivRequestDemo!!.width)
                    isAnimation = false
                } else {
                    topBottomLayoutAnimation(ll_main!!, 0)
                    isAnimation = true
                }
            }

            R.id.ivClose -> {
                topBottomLayoutAnimation(ll_main, ll_main.width - ivRequestDemo!!.width)
                isAnimation = false
            }

            R.id.ivBack -> {
                onBackPressed()
            }

            R.id.btnSubmit -> {
                validation()
            }
        }
    }

    private fun validation() {
        val getFname = etFName!!.text.toString().trim()
        val getInstitute = etinstituteName!!.text.toString().trim()
        val getPhoneNo = etPhone!!.text.toString().trim()
        val getEmail = etEmail!!.text.toString().trim()
        val getCityName = etCityName!!.text.toString().trim()

        when {
            getFname == "" -> etFName!!.error = "Please Enter First Name"
            getInstitute == "" -> etinstituteName!!.error = "Please Enter Institute Name"
            getPhoneNo == "" -> etPhone!!.error = "Please Enter Phone No"
            getPhoneNo.length < 10 -> etPhone!!.error = "Please Enter Valid Mobile No"
            getEmail == "" -> etEmail!!.error = "Please Enter Email ID"
            getCityName == "" -> etCityName!!.error = "Please Enter City Name"
            else -> {

                dbHalper!!.insertRequestDetails(RequestDetails(getFname, getInstitute, getPhoneNo, getEmail, getCityName))
                topBottomLayoutAnimation(ll_main, ll_main.width - ivRequestDemo!!.width)
                isAnimation = false
                Toast.makeText(this, "your request has been submitted successfully", Toast.LENGTH_LONG).show()
                etFName!!.setText("")
                etPhone!!.setText("")
                etEmail!!.setText("")
                etinstituteName!!.setText("")
                etCityName!!.setText("")
            }
        }


    }

    private fun topBottomLayoutAnimation(view: View, distance: Int) {
        view.animate()
                .translationX(distance.toFloat())
                .alpha(1.0f)
                .setListener(null)
    }


    fun hideKeyboard(activity: Activity) {
        val view = activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_menu -> {
                drawer_layout.openDrawer(GravityCompat.END)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

//            R.id.nav_request_a_demo -> {
////                changeFragment(ASSESSMENT_FRAGMENT)
//            }

            R.id.nav_contact -> {
                changeFragment(CONTACT_FRAGMENT)
                drawer_layout.closeDrawer(GravityCompat.END)

            }
            R.id.nav_download -> {
                /*  val intent = Intent(this, AndroidDatabaseManager::class.java)
                  startActivity(intent)
              */
                if (PermissionHelper.checkForStoragePermission(context())) {
                    exportDB()
                } else {
                    PermissionHelper.requestStoragePermission(this)
                }

            }

            R.id.nav_book_sample -> {
                changeFragment(CBSE_BOOK_SAMPLE)
                drawer_layout.closeDrawer(GravityCompat.END)
            }


            R.id.nav_request_a_demo -> {
                changeFragment(REQUEST_A_DEMO)
                drawer_layout.closeDrawer(GravityCompat.END)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.END)
        return true
    }

    private fun exportDB() {
//    val exportDir = File(Environment.getExternalStorageDirectory(), "")
        val root = Environment.getExternalStorageDirectory().absolutePath
        val myDir = File("$root/PearsonMypedia")
        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        val file = File(myDir, "PearsonMypediaDB.csv")
        try {
            file.createNewFile()
            val csvWrite = CSVWriter(FileWriter(file))
            val db = dbHalper!!.readableDatabase
            val curCSV = db.rawQuery("SELECT * FROM tabledetails", null)
            csvWrite.writeNext(curCSV.columnNames)
            while (curCSV.moveToNext()) {
                //Which column you want to exprort
                val arrStr = arrayOf<String>(curCSV.getString(0), curCSV.getString(1), curCSV.getString(2), curCSV.getString(3), curCSV.getString(4), curCSV.getString(5))
                csvWrite.writeNext(arrStr)
            }
            csvWrite.close()
            curCSV.close()
            showToast("File downloaded in " + myDir.absolutePath)
        } catch (sqlEx: Exception) {
            Log.e("MainActivity", sqlEx.message, sqlEx)
        }

    }

    private fun changeFragment(fragmentID: Int) {
        when (fragmentID) {
            HOME_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.color.home_background_color)
                val fragment = HomeFragment.newInstance("", "")
                setFragment(R.id.content, false, fragment, "Home Fragment")
            }
            ACADAMIC_PLAN_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.academic_bg)
                val fragment = AcademicPlanFragmnet.newInstance(1)
                setFragment(R.id.content, true, fragment, "AcademicPlanFragment")
            }
            ASSESSMENT_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.accessmp)
                val fragment = AssessMentFragment.newInstance(2)
                setFragment(R.id.content, true, fragment, "AssessMentFragment")
            }
            STUDENT_APPLICATION_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.student_app)
                val fragment = StudentFragment.newInstance(3)
                setFragment(R.id.content, true, fragment, "StudentFragment")
            }
            ICSE_CATEGORY_FRAGMENT -> {
                coordinatorLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.home_background_color))
                val fragment = IcseCategoryFragment.newInstance("", "")
                setFragment(R.id.content, true, fragment, "IcseCategoryFragment")
            }
            ECARD_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.ecard_back)
                val fragment = EcardFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "EcardFragment")
            }
            STUDENT_COURSE_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.studentcourse)
                val fragment = StudentCourseFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "StudentCourseFragment")
            }
            TOPIC_WISE_FRAGMENT -> {
//            if(isTab)
//                coordinatorLayout.setBackgroundResource(R.drawable.topic_wise)
                val fragment = TopicWiseFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "TopicWiseFragment")
            }
            TEACHERS_PORTAL_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.teacher_portal)
                val fragment = TeacherPortal.newInstance(1)
                setFragment(R.id.content, true, fragment, "TeacherPortal")
            }
            HOME_APP_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.home_app)
                else
                    coordinatorLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite))
                val fragment = HomeAppFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "HomeAppFragment")
            }
            ONLINE_PROFESSIONAL_DEVELOPMENT_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.onlne_pro)
                val fragment = OnlineProfessionalDevelopment.newInstance(1)
                setFragment(R.id.content, true, fragment, "OnlineProfessionalDevelopment")
            }
            IN_CLASS_DIGITAL_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.inclass)
                val fragment = DigitalResourcesFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "DigitalResourcesFragment")
            }
            CONTACT_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.contactus_bg)
                val fragment = ContactUsFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "Contactusfragment")
            }
            CBSE_BOOK_SAMPLE -> {
                coordinatorLayout.setBackgroundResource(R.color.home_background_color)
                val fragment = BookSampleFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "BookSampleFragment")
            }

            Constants.REQUEST_A_DEMO -> {
                coordinatorLayout.setBackgroundResource(R.color.home_background_color)
                val fragment = RequestADemoFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "RequestADemoFragment")
            }

            /*CBSE FRAGMENTS*/
            CBSE_CATEGORY_FRAGMENT -> {
                coordinatorLayout.setBackgroundColor(resources.getColor(R.color.home_background_color))
                val fragment = CbseCategoryFragment.newInstance("", "")
                setFragment(R.id.content, true, fragment, "CbseCategoryFragment")
            }
            CBSE_ACADAMIC_PLAN_FRAGMENT -> {
                //coordinatorLayout.setBackgroundResource(R.drawable.cbse_acedmic)
                coordinatorLayout.setBackgroundResource(R.drawable.chapter)
                val fragment = CbscAcademicFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CbscAcademicFragment")
            }
            CBSE_ASSESSMENT_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_assesment_mapped_bg)
                val fragment = CbseAssessMentFragment.newInstance(2)
                setFragment(R.id.content, true, fragment, "CBSEAssessMentFragment")
            }
            CBSE_STUDENT_APPLICATION_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_student_new)
                val fragment = CbseStudentFragment.newInstance(3)
                setFragment(R.id.content, true, fragment, "CBSEStudentFragment")
            }
            CBSE_ECARD_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_ereport_card_bg)
                val fragment = CbseEcardFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSEEcardFragment")
            }
            CBSE_STUDENT_COURSE_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_student_course_bg)
                val fragment = CbseStudentCourseBook.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSEStudentCourseFragment")
            }
            CBSE_TOPIC_WISE_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_topic_wise_sla_bg)
                val fragment = CbseTopicWise.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSETopicWiseFragment")
            }
            CBSE_TEACHERS_PORTAL_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_teacher_portal)
                val fragment = CbseTeacherPortal.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSETeacherPortal")
            }
            CBSE_HOME_APP_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_home_app_bg)
                val fragment = CbseHomeFragement.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSEHomeAppFragment")
            }
            CBSE_ONLINE_PROFESSIONAL_DEVELOPMENT_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_online)
                val fragment = CbseOnlineProfessional.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSEOnlineProfessionalDevelopment")
            }
            CBSE_IN_CLASS_DIGITAL_FRAGMENT -> {
                coordinatorLayout.setBackgroundResource(R.drawable.cbse_inclass)
                val fragment = CbseDigitalResourcesFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSEDigitalResourcesFragment")
            }
            CBSE_AR_VR_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_arvr)
                val fragment = CbseArvrFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSE_AR_VR_FRAGMENT")
            }
            Constants.CBSE_STEM_ENGLISH_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_stem)
                val fragment = CbseStemEnglishFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSE_STEM_ENGLISH_FRAGMENT")
            }
            Constants.CBSE_DYNAMIC_LESSON_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_dynamic_lesson_planning_tool)
                val fragment = CbseDynamicessonPlanningToolFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSE_DYNAMIC_LESSON_FRAGMENT")
            }
            Constants.CBSE_INSIGHT_IN_CLASS_FRAGMENT -> {
                if (isTab)
                    coordinatorLayout.setBackgroundResource(R.drawable.cbse_inside)
                val fragment = CbseInsightInClassFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSE_INSIGHT_IN_CLASS_FRAGMENT")
            }
            CBSE_CHAPTER_PLAN -> {
                coordinatorLayout.setBackgroundResource(R.drawable.chapter)
                val fragment = ChapterplanFragment.newInstance(1)
                setFragment(R.id.content, true, fragment, "CBSE_CHAPTER_PLAN")
            }
        }

    }


    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentChange(fragmentId: Int, bundle: Bundle?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        changeFragment(fragmentId)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionHelper.REQUEST_CODE_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                exportDB()
            } else {
                showToast(getString(R.string.gallery_permission_cancel))
            }
        }
    }

}