package com.pearson.mypedia;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pearson.mypedia.utility.Constants;
import com.pearson.mypedia.utility.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;


public class BaseActivity extends AppCompatActivity implements IBaseView, View.OnClickListener, Constants {


    private Gson gson;

    protected void write(String s) {
        System.out.println(s);
    }

    protected String saveImage(Bitmap finalBitmap) throws IOException {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/ECI Citizen");
        myDir.mkdirs();

        String timeStamp = Utils.dateFormat_yyyyMMdd_HHmmss.format(new Date());
        String imageFileName = "ECI_" + timeStamp + "_";
//        String fname = "Image" + ".jpg";
        String fname = imageFileName + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();

        FileOutputStream out = new FileOutputStream(file);
        finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        out.flush();
        out.close();


        return file.getAbsolutePath().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private ProgressDialog mProgressDialog;


    protected final void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
            mProgressDialog.setCancelable(false);
//            mProgressDialog.setMessage(message);
//            mProgressDialog = ProgressDialog.show(this, "", getString(R.string.loading_data_progress));
        }
//        if (message == null || message.length == 0) {
//            mProgressDialog.setMessage(getString(R.string.loading_data_progress));
//        } else {
//            mProgressDialog.setMessage(message[0]);
//        }

        mProgressDialog.show();

    }

    protected final void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    protected final void showToast(@NonNull String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public final void showSnackBar(@NonNull View view, @NonNull String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    protected void clearFragmentsBackStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected void setFragment(int containerId, boolean addToBackStack, Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .replace(containerId, fragment, tag);

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    protected final void enableActionBarHomeButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    protected final void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void showProgressDialog() {
        showProgressDialog("Please wait...");
    }

    @Override
    public void hideProgressDialog() {
        dismissProgressDialog();
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setListRefreshing(final SwipeRefreshLayout swipeRefreshLayout, final boolean refreshing) {
        if (swipeRefreshLayout == null) {
            return;
        }

        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout == null) {
                    return;
                }

                swipeRefreshLayout.setRefreshing(refreshing);
            }
        }, 100);
    }

    private Thread workerThread;
    private Handler uiHandler;
    private Runnable runnableToPerformOnUi;

    /**
     * executeOnUiThreadAfterDelay is applicable when need to execute some code on UI thread
     * after some delay (ms)
     * <p>
     * Usage example: inside of onActivityResult() where the activity state may be not restored
     */

    public void executeOnUiThreadAfterDelay(final long ms, final Runnable runnable) {
        runnableToPerformOnUi = runnable;

        if (workerThread != null && !workerThread.isInterrupted()) {
            workerThread.interrupt();
            workerThread = null;
        }

        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
        }

        uiHandler = new Handler(Looper.getMainLooper());

        workerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(ms);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                uiHandler.post(runnableToPerformOnUi);
            }
        });

        workerThread.start();
    }

    @Override
    protected void onDestroy() {
        if (workerThread != null) {
            if (!workerThread.isInterrupted()) {
                workerThread.interrupt();
            }
            workerThread = null;
        }

        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }

        dismissProgressDialog();
        super.onDestroy();
    }

    public void copyToClipBoard(String string) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", string);
        clipboard.setPrimaryClip(clip);
        showToast(string + " is copied to clipboard");
    }

    public void showDialogWithMessage(String msg) {
        if (isFinishing()) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setNegativeButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideSoftKeyboard(BaseActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void showToastMessage(String message) {
        if (this == null) {
            return;
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void gotoActivityWithResult(Class<?> classActivity, Bundle bundle, int code) {
        Intent intent = new Intent(context(), classActivity);
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle);
        startActivityForResult(intent, code);

    }

    public void gotoActivityWithFinish(Class<?> classActivity, Bundle bundle) {
        Intent intent = new Intent(context(), classActivity);
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle);
        startActivity(intent);
        finish();

    }

    public void gotoActivityWithAllFinish(Class<?> classActivity, Bundle bundle) {
        Intent intent = new Intent(context(), classActivity);
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    public void goToActivity(Class<?> classActivity, Bundle bundle) {
        Intent intent = new Intent(context(), classActivity);
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle);
        startActivity(intent);
    }


    public void gotoActivityWithClearTop(Class<?> classActivity, Bundle bundle) {
        Intent intent = new Intent(context(), classActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (bundle != null)
            intent.putExtra("android.intent.extra.INTENT", bundle);
        startActivity(intent);
    }

    public void setUpToolbar(String title, boolean isHomeAsUpEnabled) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setIcon(R.mipmap.app_icon_new);
        getSupportActionBar().setTitle("" + title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHomeAsUpEnabled);
        getSupportActionBar().setHomeButtonEnabled(isHomeAsUpEnabled);
//        getSupportActionBar().setLogo(R.drawable.logo);
    }

    public Gson getGsonInstance() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }


}
