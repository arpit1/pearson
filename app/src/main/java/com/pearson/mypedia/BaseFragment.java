package com.pearson.mypedia;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;


public class BaseFragment extends Fragment implements IBaseView, View.OnClickListener {

    protected Activity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    protected void showToastMessage(String message) {
        if (getContext() == null) {
            return;
        }
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void setActivityTitle(String title) {
        if (getActivity() == null) {
            return;
        }
        getActivity().setTitle(title);
    }

    protected void setFragment(int containerId, Fragment fragment, String tag) {
        if (getActivity() == null) {
            return;
        }
        ((BaseActivity) getActivity()).setFragment(containerId, false, fragment, tag);
    }

    protected void setChildFragment(int containerId, Fragment fragment) {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .replace(containerId, fragment);

        transaction.commit();
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Override
    public void showProgressDialog() {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).showProgressDialog();
    }

    @Override
    public void hideProgressDialog() {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).hideProgressDialog();
    }

    @Override
    public void showError(String message) {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).showError(message);
    }

    @Override
    public void gotoActivityWithFinish(Class<?> classActivity, Bundle bundle) {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).gotoActivityWithFinish(classActivity, bundle);
    }

    @Override
    public void gotoActivityWithAllFinish(Class<?> classActivity, Bundle bundle) {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).gotoActivityWithAllFinish(classActivity, bundle);
    }

    @Override
    public void goToActivity(Class<?> classActivity, Bundle bundle) {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).goToActivity(classActivity, bundle);
    }

    @Override
    public void gotoActivityWithClearTop(Class<?> classActivity, Bundle bundle) {
        if (getActivity() == null) {
            return;
        }
        ((IBaseView) getActivity()).gotoActivityWithClearTop(classActivity, bundle);
    }

    public void setListRefreshing(final SwipeRefreshLayout swipeRefreshLayout, final boolean refreshing) {
        if (swipeRefreshLayout == null) {
            return;
        }

        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout == null) {
                    return;
                }

                swipeRefreshLayout.setRefreshing(refreshing);
            }
        }, 100);
    }

    public void executeOnUiThreadAfterDelay(long ms, Runnable runnable) {
        ((BaseActivity) getActivity()).executeOnUiThreadAfterDelay(ms, runnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(BuildConfig.LEAK_CANARY_ENABLED) {
//            RefWatcher refWatcher = ChurchBlazeApp.getRefWatcher(getActivity());
//            refWatcher.watch(this);
//        }
    }


    protected final void showShortToast(@NonNull String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public Gson getGsonInstance() {
        if (getActivity() == null) {
            return null;
        }
        return ((IBaseView) getActivity()).getGsonInstance();
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == PermissionHelper.REQUEST_CODE_CAMERA) {
//            if (grantResults.length > 0
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//            } else {
//                Toast.makeText(context(), "Cancelling because Storage permissions are not granted", Toast.LENGTH_LONG).show();
//            }
//        }
//        if (requestCode == PermissionHelper.REQUEST_CODE_STORAGE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // required permissions granted, start crop image activity
//
//            } else {
//                Toast.makeText(context(), "Cancelling because Camera permissions are not granted", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
}
