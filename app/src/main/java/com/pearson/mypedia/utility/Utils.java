package com.pearson.mypedia.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import com.pearson.mypedia.BaseActivity;
import com.pearson.mypedia.R;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by kaushalpanda on 1/9/18.
 */

public class Utils {

    public static DateFormat dateFormat_yyyyMMdd_HHmmss =
            new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH);

    /**
     * Check Internet connection available or not.
     *
     * @param context : current activity context
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    /**
     * Open Dialog and Settings Internet connection available or not.
     *
     * @param mContext : current activity context
     * @return boolean
     */

    public static void displayAlert(final Context mContext) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle("Network Error");
        alertDialog.setMessage("Would you like to turn on data!");
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.fail);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if (((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public static void showSoftKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

    public static void shareTheApp(Context context) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
        String sAux = "\nLet me recommend you this application\n\n";
        sAux = sAux
                + "https://play.google.com/store/apps/details?id=" + context.getPackageName();
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        context.startActivity(Intent.createChooser(i, "Share via"));
    }


    /**
     * Convert DP into PIXEL
     * put DP value in @param dp
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * get millisecond from time's string
     */
    public static long getDateTimeMillisFromString(String myTimestamp) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        form.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = form.parse(myTimestamp);
            long millis = Calendar.getInstance().getTime().getTime() - date.getTime();
            System.out.println("Date  : " + millis);
//            System.out.println("Date  : " + new Date(millis).toString());
            return millis;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static boolean checkEmailPattern(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean checkMobilePattern(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public static void showConfirmationDialog(final Context context, String message) {
        new AlertDialog.Builder(context).setMessage(message).setTitle("Confirmation").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ((BaseActivity) context).finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }



    public static String GetHash(String input, String secureKey) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
        }
        byte[] digest = m.digest((input.trim() + secureKey.trim()).getBytes(Charset.forName("UTF-8")));
        String hash = new BigInteger(1, digest).toString(16);
        return hash.toString();
    }

    public static boolean isValidString(String str)
    {
        boolean isValid = false;
        String expression = "^[a-z_A-Z0-9 ]*$";
        CharSequence inputStr = str;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if(matcher.matches())
        {
            isValid = true;
        }
        return isValid;
    }

 public static Bitmap getBitmap(Context context, int drawableRes) {
        Drawable drawable = context.getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static File createImageFile(Activity activity) throws IOException {
        String timeStamp = dateFormat_yyyyMMdd_HHmmss.format(new Date());
        String imageFileName = "ECI_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        return image;
    }

    public static boolean checkSupportedFromat(String attachmentPath) {
        if (/*attachmentPath.contains(".pdf") ||
                attachmentPath.contains(".doc") ||*/
                attachmentPath.toLowerCase().contains(".png") ||
                        attachmentPath.toLowerCase().contains(".jpg") ||
                        attachmentPath.toLowerCase().contains(".jpeg"))


            return true;
        else return false;
    }

//    public static File createVideoFile(Activity activity) throws IOException {
//        String timeStamp = dateFormat_yyyyMMdd_HHmmss.format(new Date());
//        String imageFileName = "MP4_" + timeStamp + "_";
//        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_MOVIES);
//        File video = File.createTempFile(imageFileName, ".mp4", storageDir);
//        return video;
//    }
}
