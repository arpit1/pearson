package com.pearson.mypedia.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.pearson.mypedia.BaseActivity;
import com.pearson.mypedia.BaseFragment;

import java.util.ArrayList;
import java.util.List;


public class PermissionHelper {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static final int REQUEST_CODE_STORAGE = 101;
    public static final int REQUEST_CODE_CAMERA = 102;
    public static final int REQUEST_CODE_CALL = 103;
    public static final int REQUEST_CODE_LOCATION = 103;

    public static boolean checkForStoragePermission(Context context) {
        int readPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);

        int writePermissionCheck = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return readPermissionCheck == PackageManager.PERMISSION_GRANTED && writePermissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkForCameraPermission(Context context) {
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int writePermissionCheck = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);


        return cameraPermissionCheck == PackageManager.PERMISSION_GRANTED && writePermissionCheck == PackageManager.PERMISSION_GRANTED;

    }

    public static void requestStoragePermission(BaseFragment activity) {
        activity.requestPermissions(
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                REQUEST_CODE_STORAGE);
    }


    public static void requestStoragePermission(BaseActivity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                REQUEST_CODE_STORAGE);
    }

    public static void requestCameraPermission(BaseActivity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_CAMERA);
    }
    public static void requestCameraPermission(BaseFragment activity) {
        activity.requestPermissions(
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_CAMERA);
    }

    public static boolean checkCallPermission(Context context) {
        int callPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);


        return callPermissionCheck == PackageManager.PERMISSION_GRANTED;

    }

    public static void requestCallPermission(BaseActivity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CALL_PHONE
                },
                REQUEST_CODE_CALL);
    }

    public static void requestCallPermission(Fragment frag) {
        frag.requestPermissions(
                new String[]{Manifest.permission.CALL_PHONE
                },
                REQUEST_CODE_CALL);
    }

    public static boolean checkLocationPermission(Context context) {
        int locationPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        return locationPermissionCheck == PackageManager.PERMISSION_GRANTED;

    }

//    public static void requestLocationPermission(MeetingDetailActivity activity) {
//        ActivityCompat.requestPermissions(activity,
//                new String[]{Manifest.permission.ACCESS_FINE_LOCATION
//                },
//                REQUEST_CODE_LOCATION);
//    }

    public static boolean checkAndRequestPermissions(Context context) {
        int camerapermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readpermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


}
