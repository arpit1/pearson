package com.pearson.mypedia.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.pearson.mypedia.R
import com.pearson.mypedia.database.DBHalper
import com.pearson.mypedia.interfaces.OnFragmentInteractionListener
import com.pearson.mypedia.models.RequestDetails
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.custom_dialog.*
import java.util.ArrayList

class RequestADemoFragment : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    var etFName: EditText? = null
    var etinstituteName: EditText? = null
    var etPhone: EditText? = null
    var etEmail: EditText? = null
    var etCityName: EditText? = null
    var dbHalper: DBHalper? = null
    var btnSubmit: Button? = null
    internal var requestDetailsList: List<RequestDetails> = ArrayList()

    companion object {
        @JvmStatic
        fun newInstance(columnCount: Int): RequestADemoFragment {
            val fragment = RequestADemoFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_request_demo, container, false)

        // Inflate the layout for this fragment
        dbHalper = DBHalper(activity)
        initUI(view)
        btnSubmit!!.setOnClickListener(this)
        return view
    }

    // TODO: Rename method, update argument and hook method into UI event

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    fun initUI(view: View) {
        etFName = view.findViewById(R.id.frag_etFName)
        etinstituteName = view.findViewById(R.id.frag_etinstituteName)
        etPhone = view.findViewById(R.id.frag_etPhone)
        etEmail = view.findViewById(R.id.frag_etEmail)
        etCityName = view.findViewById(R.id.frag_etCityName)
        btnSubmit = view.findViewById(R.id.frag_btnSubmit)


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSubmit -> {
                validation()
            }
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    private fun validation() {
        val getFname = etFName!!.text.toString().trim()
        val getInstitute = etinstituteName!!.text.toString().trim()
        val getPhoneNo = etPhone!!.text.toString().trim()
        val getEmail = etEmail!!.text.toString().trim()
        val getCityName = etCityName!!.text.toString().trim()

        when {
            getFname == "" -> etFName!!.error = "Please Enter First Name"
            getInstitute == "" -> etinstituteName!!.error = "Please Enter Institute Name"
            getPhoneNo == "" -> etPhone!!.error = "Please Enter Phone No"
            getPhoneNo.length < 10 -> etPhone!!.error = "Please Enter Valid Mobile No"
            getEmail == "" -> etEmail!!.error = "Please Enter Email ID"
            getCityName == "" -> etCityName!!.error = "Please Enter City Name"
            else -> {

                dbHalper!!.insertRequestDetails(RequestDetails(getFname, getInstitute, getPhoneNo, getEmail, getCityName))
                Toast.makeText(activity, "your request has been submitted successfully", Toast.LENGTH_LONG).show()
                etFName!!.setText("")
                etPhone!!.setText("")
                etEmail!!.setText("")
                etinstituteName!!.setText("")
                etCityName!!.setText("")
            }
        }


    }
}
