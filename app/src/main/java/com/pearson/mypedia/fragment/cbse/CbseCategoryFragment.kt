package com.pearson.mypedia.fragment.cbse

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.OvershootInterpolator
import android.view.animation.ScaleAnimation
import android.widget.AdapterView
import com.pearson.mypedia.R
import com.pearson.mypedia.interfaces.OnFragmentInteractionListener
import com.pearson.mypedia.utility.Constants
import kotlinx.android.synthetic.main.cbse_circle_layout.*
import kotlinx.android.synthetic.main.cbse_circle_layout.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [IcseCategoryFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [IcseCategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CbseCategoryFragment : Fragment(), View.OnClickListener {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var isCourseworkAnim: Boolean = true
    private var isAssessmentAnim: Boolean = true
    private var isDigitalAnim: Boolean = true
    private var mScaleValue: Float = 1.3f
    private var isAnimateFabDown = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.cbse_circle_layout, container, false)
//        initUI(view)
        clickListner(view)
        return view
    }

    fun initUI(view: View) {
//        btnIcse = view!!.findViewById(R.id.btn_icse)
//        viewicse = view!!.findViewById(R.id.icse_circle_layout)
    }

    fun clickListner(view: View) {
        view.llStudentCourseBook!!.setOnClickListener(this)
        view.llStudentApplicationBook!!.setOnClickListener(this)
        view.llAcademicPlan!!.setOnClickListener(this)
        view.llAssessmentMapped!!.setOnClickListener(this)
        view.llTopicWise!!.setOnClickListener(this)
        view.llEReportCards!!.setOnClickListener(this)
        view.llInClassDigital!!.setOnClickListener(this)
        view.llTeacherPortal!!.setOnClickListener(this)
        view.llOnlineProfessionalDevelopment!!.setOnClickListener(this)
        view.llHomeApp!!.setOnClickListener(this)

        view.llCourseWareSmallCircle!!.setOnClickListener(this)
        view.llAssessmentSmallCircle!!.setOnClickListener(this)
        view.llDigitalSmallCircle!!.setOnClickListener(this)

        view.tvARVR!!.setOnClickListener(this)
        view.tvStemEnglish!!.setOnClickListener(this)
        view.tvDynamicLesson!!.setOnClickListener(this)
        view.tvInsightInClass!!.setOnClickListener(this)

        view.fabScroll!!.setOnClickListener(this)

        view.viewChapterPlan!!.setOnClickListener(this)


        view.spinnerViewAs?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, v: View?, position: Int, id: Long) {
                view.llStudentCourseBook!!.visibility = if (position == 0 || position == 1 || position == 2 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llStudentApplicationBook!!.visibility = if (position == 0 || position == 1 || position == 2 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llAcademicPlan!!.visibility = if (position == 0 || position == 1 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llAssessmentMapped!!.visibility = if (position == 0 || position == 1 || position == 2 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llTopicWise!!.visibility = if (position == 0 || position == 1) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llEReportCards!!.visibility = if (position == 0 || position == 2 || position == 3 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llInClassDigital!!.visibility = if (position == 0 || position == 1) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llTeacherPortal!!.visibility = if (position == 0) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llOnlineProfessionalDevelopment!!.visibility = if (position == 0 || position == 1 || position == 4) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.llHomeApp!!.visibility = if (position == 0 || position == 1 || position == 2 || position == 3) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }

                view.tvARVR!!.visibility = if (position == 0 || position == 2) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.tvStemEnglish!!.visibility = if (position == 0 || position == 2) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.tvDynamicLesson!!.visibility = if (position == 0 || position == 1) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
                view.tvInsightInClass!!.visibility = if (position == 0) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
            }

        }


//    btnIcse!!.setOnClickListener(View.OnClickListener { showIcse() })
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.llStudentCourseBook -> {
                listener?.onFragmentChange(Constants.CBSE_STUDENT_COURSE_FRAGMENT, null)
            }

            R.id.llStudentApplicationBook ->
                listener?.onFragmentChange(Constants.CBSE_STUDENT_APPLICATION_FRAGMENT, null)

            R.id.llAcademicPlan ->
                listener?.onFragmentChange(Constants.CBSE_ACADAMIC_PLAN_FRAGMENT, null)

            R.id.llAssessmentMapped ->
                listener?.onFragmentChange(Constants.CBSE_ASSESSMENT_FRAGMENT, null)

            R.id.llTopicWise -> {
                listener?.onFragmentChange(Constants.CBSE_TOPIC_WISE_FRAGMENT, null)
            }

            R.id.llEReportCards ->
                listener?.onFragmentChange(Constants.CBSE_ECARD_FRAGMENT, null)

            R.id.llInClassDigital -> {
                listener?.onFragmentChange(Constants.CBSE_IN_CLASS_DIGITAL_FRAGMENT, null)
            }


            R.id.llTeacherPortal -> {
                listener?.onFragmentChange(Constants.CBSE_TEACHERS_PORTAL_FRAGMENT, null)
            }

            R.id.llOnlineProfessionalDevelopment -> {
                listener?.onFragmentChange(Constants.CBSE_ONLINE_PROFESSIONAL_DEVELOPMENT_FRAGMENT, null)
            }

            R.id.llHomeApp -> {
                listener?.onFragmentChange(Constants.CBSE_HOME_APP_FRAGMENT, null)
            }

            R.id.tvARVR -> {
                listener?.onFragmentChange(Constants.CBSE_AR_VR_FRAGMENT, null)
            }

            R.id.tvStemEnglish -> {
                listener?.onFragmentChange(Constants.CBSE_STEM_ENGLISH_FRAGMENT, null)
            }

            R.id.tvDynamicLesson -> {
                listener?.onFragmentChange(Constants.CBSE_DYNAMIC_LESSON_FRAGMENT, null)
            }

            R.id.tvInsightInClass -> {
                listener?.onFragmentChange(Constants.CBSE_INSIGHT_IN_CLASS_FRAGMENT, null)
            }


            R.id.llCourseWareSmallCircle -> {
                if (isCourseworkAnim) {
                    scaleUpAnimation(llCourseWareSmallCircle, 1f, mScaleValue)
                    isCourseworkAnim = false
                } else {
                    scaleUpAnimation(llCourseWareSmallCircle, mScaleValue, 1f)
                    isCourseworkAnim = true
                }

            }

            R.id.llAssessmentSmallCircle -> {


                if (isAssessmentAnim) {
                    scaleUpAnimation(llAssessmentSmallCircle, 1f, mScaleValue)
                    isAssessmentAnim = false
                } else {
                    scaleUpAnimation(llAssessmentSmallCircle, mScaleValue, 1f)
                    isAssessmentAnim = true
                }
            }

            R.id.llDigitalSmallCircle -> {

                if (isDigitalAnim) {
                    scaleUpAnimation(llDigitalSmallCircle, 1f, mScaleValue)
                    isDigitalAnim = false
                } else {
                    scaleUpAnimation(llDigitalSmallCircle, mScaleValue, 1f)
                    isDigitalAnim = true
                }
            }

            R.id.fabScroll -> {
                if (isAnimateFabDown) {
                    val interpolator = OvershootInterpolator()
                    ViewCompat.animate(fabScroll).rotation(180f).withLayer().setDuration(500).setInterpolator(interpolator).start()
                    scrollView.smoothScrollTo(0, scrollView.bottom)
                    isAnimateFabDown = false
                } else {
                    val interpolator = OvershootInterpolator()
                    ViewCompat.animate(fabScroll).rotation(0f).withLayer().setDuration(500).setInterpolator(interpolator).start()
                    scrollView.smoothScrollTo(0, scrollView.top)
                    isAnimateFabDown = true
                }
            }

            R.id.viewChapterPlan -> {
                listener?.onFragmentChange(Constants.CBSE_CHAPTER_PLAN, null)
            }


        }
    }

//    override fun onClick(v: View?) {
//        super.onClick(v)
//        when(v!!.id)
//        {
//            R.id.btn_icse ->
//            {
//                listener?.onFragmentChange(Constants.ICSE_CATEGORY_FRAGMENT, null)
//            }
//        }
//
//    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment IcseCategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                CbseCategoryFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    fun scaleUpAnimation(v: View, startScale: Float, endScale: Float) {
        val anim = ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.fillAfter = true; // Needed to keep the result of the animation
        anim.duration = 500;
        v.startAnimation(anim);
    }
}
