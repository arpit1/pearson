package com.pearson.mypedia.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.pearson.mypedia.BaseFragment
import com.pearson.mypedia.R
import com.pearson.mypedia.interfaces.OnFragmentInteractionListener
import com.pearson.mypedia.utility.Constants.CBSE_CATEGORY_FRAGMENT
import com.pearson.mypedia.utility.Constants.ICSE_CATEGORY_FRAGMENT

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class HomeFragment : BaseFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var btnIcse: Button? = null
    private var btnCbse: Button? = null
//    private var viewicse: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Inflate the layout for this fragment

        initUI(view)
        clickListner()
        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }
    fun initUI(view: View)
    {
        btnIcse = view.findViewById(R.id.btn_icse)
        btnCbse = view.findViewById(R.id.btn_cbse)

//        viewicse = view!!.findViewById(R.id.icse_circle_layout)
    }
fun clickListner()
{
    btnIcse!!.setOnClickListener(this)
    btnCbse!!.setOnClickListener(this)
//    btnIcse!!.setOnClickListener(View.OnClickListener { showIcse() })
}

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v!!.id)
        {
            R.id.btn_icse ->
            {
                listener?.onFragmentChange(ICSE_CATEGORY_FRAGMENT, null)

            }

            R.id.btn_cbse -> {
                listener?.onFragmentChange(CBSE_CATEGORY_FRAGMENT, null)
            }
        }

    }

    fun showIcse()
    {
//      viewicse!!.visibility = View.VISIBLE
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                HomeFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
