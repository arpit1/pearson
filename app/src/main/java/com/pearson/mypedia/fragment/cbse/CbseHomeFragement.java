package com.pearson.mypedia.fragment.cbse;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pearson.mypedia.BaseFragment;
import com.pearson.mypedia.R;
import com.pearson.mypedia.activity.FullImageViewActivity;
import com.pearson.mypedia.database.DBHalper;

import java.io.Serializable;
import java.util.ArrayList;

import static com.pearson.mypedia.activity.FullImageViewActivity.PARAM_IMAGES;

public class CbseHomeFragement  extends BaseFragment implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{

    MediaPlayer player;
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private FloatingActionButton mPlayPauseButton, fabScreenshots;

    // TODO: Customize parameters
    private int mColumnCount = 1;
    Button showbootamn;
    //  private BottamSheet bottamSheet;
    public CbseHomeFragement() {
        // Required empty public constructor
    }
    DBHalper dbHalper;


    private ArrayList<Integer> imgPathList;

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CbseHomeFragement newInstance(int columnCount) {
        CbseHomeFragement fragment = new CbseHomeFragement();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.cbse_home_fragment, container, false);
        mPlayPauseButton = view.findViewById(R.id.fabPlayPause);
        fabScreenshots = view.findViewById(R.id.fabScreenshots);
        clickListner(view);

        setScreenShotImages();

        int rawVideo = R.raw.cbse_home_app;
        try {

//            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            String uriPath = "android.resource://" + context().getPackageName() + "/" + rawVideo;
//            player.setDataSource(getContext(), Uri.parse(uriPath));
            player = MediaPlayer.create(context(), Uri.parse(uriPath));
            player.setOnPreparedListener(this);
            player.setOnErrorListener(this);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mPlayPauseButton.setImageResource(R.mipmap.ic_play_button);
                    myPauseCondition();
                    player.seekTo(1);
                }
            });
            player.start();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Inflate the layout for this fragment
        return view;
    }

    private void setScreenShotImages() {
        imgPathList = new ArrayList<>();
        imgPathList.add(R.drawable.home_app_screenshot_1);
        imgPathList.add(R.drawable.home_app_screenshot_2);
        imgPathList.add(R.drawable.home_app_screenshot_3);
        imgPathList.add(R.drawable.home_app_screenshot_4);
        imgPathList.add(R.drawable.home_app_screenshot_5);
        imgPathList.add(R.drawable.home_app_screenshot_6);
        imgPathList.add(R.drawable.home_app_screenshot_7);
        imgPathList.add(R.drawable.home_app_screenshot_8);
        imgPathList.add(R.drawable.home_app_screenshot_9);
        imgPathList.add(R.drawable.home_app_screenshot_10);
        imgPathList.add(R.drawable.home_app_screenshot_11);
        imgPathList.add(R.drawable.home_app_screenshot_12);
        imgPathList.add(R.drawable.home_app_screenshot_13);
        imgPathList.add(R.drawable.home_app_screenshot_14);

    }


    private void clickListner(View view) {
        view.findViewById(R.id.fabPlayPause).setOnClickListener(this);
        fabScreenshots.setOnClickListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        myPauseCondition();
    }

    @Override
    public void onResume() {
        super.onResume();
        myPlayCondition();
    }

    private void myPlayCondition() {
        if (player != null) {
            if (!player.isPlaying()) {
                player.start();
                mPlayPauseButton.setImageResource(R.mipmap.ic_pause_button);
            }
//            player.release();
        }
    }

    private void myPauseCondition() {
        if (player != null) {
            if (player.isPlaying()) {
                player.pause();
                mPlayPauseButton.setImageResource(R.mipmap.ic_play_button);
//                player.seekTo(1);
            }
//            player.release();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v.getId()==R.id.fabPlayPause) {
            if(player!=null && !player.isPlaying()) {
                myPlayCondition();
            }else{
                myPauseCondition();
            }
        }

        if(v.getId()==R.id.fabScreenshots) {
            Intent intent = new Intent(context(), FullImageViewActivity.class);
            intent.putExtra(PARAM_IMAGES, (Serializable) imgPathList);
            startActivity(intent);
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int which, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if(player!=null)
            player.seekTo(1);
    }



}
