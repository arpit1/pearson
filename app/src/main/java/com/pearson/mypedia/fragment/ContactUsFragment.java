package com.pearson.mypedia.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pearson.mypedia.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;
    SupportMapFragment mapFragment;

    public ContactUsFragment() {
        // Required empty public constructor
    }
    public static ContactUsFragment newInstance(int columnCount) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.googleMap);
        mapFragment.getMapAsync(this);

        setmap();
        return view;
    }
    public void setmap()
    {



    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(28.5769648, 77.3149534))
                .title("15th Floor, Tower-B, World Trade Tower,\n" +
                        "Plot – C01, Sector-16, Noida – 201 309 \n" +
                        "Uttar Pradesh, India \n" +
                        "Telephone: +91 (120) 4306550 \n" +
                        "Fax: +91 (120) 4306500").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(28.5769648, 77.3149534), 10));
        googleMap.getUiSettings().setMapToolbarEnabled(true);
    }
}