package com.pearson.mypedia.fragment.cbse;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pearson.mypedia.BaseFragment;
import com.pearson.mypedia.R;

public class CbseEcardFragment extends BaseFragment implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{

    MediaPlayer player;
    //    Button showbootamn;
    private static final String ARG_COLUMN_COUNT = "column-count";
    private FloatingActionButton mPlayPauseButton;

    // TODO: Customize parameters
    private int mColumnCount = 1;
    //  private BottamSheet bottamSheet;
    //  private BottamSheet bottamSheet;
    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CbseEcardFragment newInstance(int columnCount) {
        CbseEcardFragment fragment = new CbseEcardFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.cbse_ecard_layout, container, false);
        mPlayPauseButton = ((FloatingActionButton) view.findViewById(R.id.fabPlayPause));
        clickListner(view);

        int rawVideo = R.raw.cbse_e_report_cards;
        try {

//            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            String uriPath = "android.resource://" + context().getPackageName() + "/" + rawVideo;
//            player.setDataSource(getContext(), Uri.parse(uriPath));
            player = MediaPlayer.create(context(), Uri.parse(uriPath));
            player.setOnPreparedListener(this);
            player.setOnErrorListener(this);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mPlayPauseButton.setImageResource(R.mipmap.ic_play_button);
                    myPauseCondition();
                    player.seekTo(1);
                }
            });
            player.start();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Inflate the layout for this fragment
        return view;
    }

    private void clickListner(View view) {
        view.findViewById(R.id.fabPlayPause).setOnClickListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        myPauseCondition();
    }

    @Override
    public void onResume() {
        super.onResume();
        myPlayCondition();
    }

    private void myPlayCondition() {
        if (player != null) {
            if (!player.isPlaying()) {
                player.start();
                mPlayPauseButton.setImageResource(R.mipmap.ic_pause_button);
            }
//            player.release();
        }
    }

    private void myPauseCondition() {
        if (player != null) {
            if (player.isPlaying()) {
                player.pause();
                mPlayPauseButton.setImageResource(R.mipmap.ic_play_button);
//                player.seekTo(1);
            }
//            player.release();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v.getId()==R.id.fabPlayPause) {
            if(player!=null && !player.isPlaying()) {
                myPlayCondition();
            }else{
                myPauseCondition();
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int which, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if(player!=null)
            player.seekTo(1);
    }



}
