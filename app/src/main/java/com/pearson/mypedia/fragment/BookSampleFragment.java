package com.pearson.mypedia.fragment;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pearson.mypedia.BaseFragment;
import com.pearson.mypedia.R;
import com.pearson.mypedia.models.BookSampleModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookSampleFragment extends BaseFragment {


    @BindView(R.id.llHindiTrade)
    LinearLayout llHindiTrade;

    @BindView(R.id.llSSTGrade)
    LinearLayout llSSTGrade;

    @BindView(R.id.llScienceGrade)
    LinearLayout llScienceGrade;

    @BindView(R.id.llLessonPlan)
    LinearLayout llLessonPlan;

    private ArrayList<BookSampleModel> mHindiTradeArrayList;
    private ArrayList<BookSampleModel> mSSTGradeArrayList;
    private ArrayList<BookSampleModel> mScienceGradeArrayList;
    private ArrayList<BookSampleModel> mLessonPlanArrayList;


    public BookSampleFragment() {
        // Required empty public constructor
    }
    public static BookSampleFragment newInstance(int columnCount) {
        BookSampleFragment fragment = new BookSampleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book_sample, container, false);
        ButterKnife.bind(this, view);
        bindViews(view);
        return view;
    }

    private void bindViews(View view) {
        mHindiTradeArrayList = new ArrayList<>();
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 3", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-3-Binder.pdf"));
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 4", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-4-Binder.pdf"));
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 5", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-5-Binder.pdf"));
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 6", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-6-Binder.pdf"));
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 7", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-7-Binder.pdf"));
        mHindiTradeArrayList.add(new BookSampleModel("Hindi Grade 8", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Hindi-Grade-8-Binder.pdf"));

        setDataOnLayout(llHindiTrade, mHindiTradeArrayList);

        mSSTGradeArrayList = new ArrayList<>();
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 1", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-1-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 2", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-2-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 3", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-3-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 4", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-4-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 5", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-5-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 6", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-6-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 7", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-7-Binder.pdf"));
        mSSTGradeArrayList.add(new BookSampleModel("SST Grade 8", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-8-Binder.pdf"));

        setDataOnLayout(llSSTGrade, mSSTGradeArrayList);

        mScienceGradeArrayList = new ArrayList<>();
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 3", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-3-Binder.pdf"));
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 4", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-4-Binder.pdf"));
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 5", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-5-Binder.pdf"));
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 6", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-6-Binder.pdf"));
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 7", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-7-Binder.pdf"));
        mScienceGradeArrayList.add(new BookSampleModel("Science Grade 8", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/Science-Grade-8-Binder.pdf"));

        setDataOnLayout(llScienceGrade, mScienceGradeArrayList);

        mLessonPlanArrayList = new ArrayList<>();
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 1", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-1-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 2", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-2-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 3", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-3-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 4", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-4-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 5", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-5-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 6", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-6-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 7", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-7-Binder.pdf"));
        mLessonPlanArrayList.add(new BookSampleModel("SST Grade 8", "http://vermmillion.com/mypediademo/wp-content/uploads/2019/pdf/SST-Grade-8-Binder.pdf"));

        setDataOnLayout(llLessonPlan, mLessonPlanArrayList);

    }

    private void setDataOnLayout(LinearLayout llLayout, final ArrayList<BookSampleModel> modelArrayList) {
        LayoutInflater inflater = (LayoutInflater) context().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0; i<modelArrayList.size(); i++){
            View viewInflate = inflater.inflate(R.layout.book_sample_item_layout, null);
            TextView tvTitle = viewInflate.findViewById(R.id.tvTitle);

            tvTitle.setText(modelArrayList.get(i).getTitle());

            final int finalI = i;
            viewInflate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(modelArrayList.get(finalI).getUrl());
                }
            });

            llLayout.addView(viewInflate);
        }
    }

    private void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);

//        Intent target = new Intent(Intent.ACTION_VIEW);
//        target.setDataAndType(Uri.parse(url),"application/pdf");
//        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//
//        Intent intent = Intent.createChooser(target, "Open File");
//        try {
//            startActivity(intent);
//        } catch (ActivityNotFoundException e) {
//            // Instruct the user to install a PDF reader here, or something
//        }
    }

}