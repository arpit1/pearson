package com.pearson.mypedia;


import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;

public interface IBaseView {

    Context context();

    void showProgressDialog();
    void hideProgressDialog();
    void showError(String message);
    Gson getGsonInstance();
    void gotoActivityWithFinish(Class<?> classActivity, Bundle bundle);
    void gotoActivityWithAllFinish(Class<?> classActivity, Bundle bundle);
    void goToActivity(Class<?> classActivity, Bundle bundle);
    void gotoActivityWithClearTop(Class<?> classActivity, Bundle bundle);

}
