package com.pearson.mypedia.models;

public class RequestDetails {


    String fName;
    String instituteName;
    String phonrNo;
    String email;
    String cityName;

    public RequestDetails(String fName,  String instituteName, String phonrNo, String email, String cityName) {
        this.fName = fName;
        this.instituteName = instituteName;
        this.phonrNo = phonrNo;
        this.email = email;
        this.cityName = cityName;
    }

    public RequestDetails() {
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getPhonrNo() {
        return phonrNo;
    }

    public void setPhonrNo(String phonrNo) {
        this.phonrNo = phonrNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
