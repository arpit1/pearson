package com.pearson.mypedia.models;

public class BookSampleModel {

    private String title;
    private String url;

    public BookSampleModel(String title, String url){
        this.title = title;
        this.url = url;
    }


    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
}
