package com.pearson.mypedia.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pearson.mypedia.models.RequestDetails;

import java.util.ArrayList;

public class DBHalper extends SQLiteOpenHelper {

    private Context context;
    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "pearsonmedia";

    private static final String FRIST_NAME = "fname";
    private static final String INSTITUTE_NAME = "institutename";
    private static final String PHONE_NO = "phonrno";
    private static final String EMIL_ID = "email";
    private static final String CITY_NAME = "city";


    private static final String KEY_ID = "keyId";
    private static final String TABLE_DETAILS = "tabledetails";

    // task TAble
    private static final String CREATE_TABLE_DETAILS="CREATE TABLE IF NOT EXISTS "
            + TABLE_DETAILS + "(" + KEY_ID + " INTEGER PRIMARY KEY ," + FRIST_NAME + " TEXT," + INSTITUTE_NAME + " TEXT," +  PHONE_NO + " TEXT," + EMIL_ID + " TEXT," + CITY_NAME + " TEXT " + ")";




    public DBHalper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHalper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_DETAILS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
    }

    public void insertRequestDetails(RequestDetails requestDetails)
    {
        SQLiteDatabase dbb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FRIST_NAME, requestDetails.getfName());
        contentValues.put(INSTITUTE_NAME, requestDetails.getInstituteName());
        contentValues.put(PHONE_NO,requestDetails.getPhonrNo());
        contentValues.put(EMIL_ID,requestDetails.getEmail());
        contentValues.put(CITY_NAME,requestDetails.getCityName());
        dbb.insert(TABLE_DETAILS, null , contentValues);
        dbb.close();
    }

    @SuppressLint("Assert")
    public ArrayList<RequestDetails> getoutletRequestList() {
        ArrayList<RequestDetails> requestrecored = new ArrayList<RequestDetails>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DETAILS ;
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                RequestDetails requestDetails = new RequestDetails();
                requestDetails.setfName(cursor.getString(0));
                requestDetails.setInstituteName(cursor.getString(1));
                requestDetails.setPhonrNo(cursor.getString(2));
                requestDetails.setEmail(cursor.getString(3));
                requestDetails.setCityName(cursor.getString(4));
                requestrecored.add(requestDetails);
            }while (cursor.moveToNext());
        }
        return requestrecored;
    }
//public Cursor getRequestDetails() {
//    SQLiteDatabase db = this.getReadableDatabase();
//    Cursor res = db.rawQuery("select * from " + TABLE_DETAILS + " ",
//            null);
//    return res;
//}
    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"mesage"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            //     DeBug.showLog("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {

            ////      DeBug.showLog("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }
}
